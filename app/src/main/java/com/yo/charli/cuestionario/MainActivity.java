package com.yo.charli.cuestionario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    RadioGroup opciones1, opciones2, opciones3, opciones4, opciones5, opciones6, opciones7, opciones8, opciones9, opciones10;
    Switch r11o1;
    CheckBox r12o1, r12o2, r12o3, r13o1, r13o2, r13o3, r14o1, r14o2, r14o3, r15o1, r15o2, r15o3, r16o1, r16o2, r16o3, r17o1, r17o2, r17o3;
    ToggleButton r18o1, r19o1, r20o1;
    TextView resultado;
    int result=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        opciones1 = findViewById(R.id.opcionesp1);
        opciones2 = findViewById(R.id.opcionesp2);
        opciones3 = findViewById(R.id.opcionesp3);
        opciones4 = findViewById(R.id.opcionesp4);
        opciones5 = findViewById(R.id.opcionesp5);
        opciones6 = findViewById(R.id.opcionesp6);
        opciones7 = findViewById(R.id.opcionesp7);
        opciones8 = findViewById(R.id.opcionesp8);
        opciones9 = findViewById(R.id.opcionesp9);
        opciones10 = findViewById(R.id.opciones10);
        r11o1 = findViewById(R.id.r11o1);
        r11o1.setOnCheckedChangeListener(this);
        r12o1 = findViewById(R.id.r12o1);
        r12o2 = findViewById(R.id.r12o2);
        r12o3 = findViewById(R.id.r12o3);
        r13o1 = findViewById(R.id.r13o1);
        r13o2 = findViewById(R.id.r13o2);
        r13o3 = findViewById(R.id.r13o3);
        r14o1 = findViewById(R.id.r14o1);
        r14o2 = findViewById(R.id.r14o2);
        r14o3 = findViewById(R.id.r14o3);
        r15o1 = findViewById(R.id.r15o1);
        r15o2 = findViewById(R.id.r15o2);
        r15o3 = findViewById(R.id.r15o3);
        r16o1 = findViewById(R.id.r16o1);
        r16o2 = findViewById(R.id.r16o2);
        r16o3 = findViewById(R.id.r16o3);
        r17o1 = findViewById(R.id.r17o1);
        r17o2 = findViewById(R.id.r17o2);
        r17o3 = findViewById(R.id.r17o3);
        r18o1 = findViewById(R.id.r18o1);
        r18o1.setOnCheckedChangeListener(this);
        r19o1 = findViewById(R.id.r19o1);
        r19o1.setOnCheckedChangeListener(this);
        r20o1 = findViewById(R.id.r20o1);
        r20o1.setOnCheckedChangeListener(this);
        resultado = findViewById(R.id.resultado);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.r11o1:
                result = r11o1.isChecked() ? result+1: result-1;
                break;
            case R.id.r18o1:
                result = r18o1.isChecked() ? result+1: result-1;
                break;
            case R.id.r19o1:
                result = r19o1.isChecked() ? result-1: result+1;
                break;
            case R.id.r20o1:
                result = r20o1.isChecked() ? result+1: result-1;
                break;
        }
    }

    public void verificar(View view){
        result = opciones1.getCheckedRadioButtonId() == R.id.r1o3 ? result+1: result-1;
        result = opciones2.getCheckedRadioButtonId() == R.id.r2o1 ? result+1: result-1;
        result = opciones3.getCheckedRadioButtonId() == R.id.r3o1 ? result+1: result-1;
        result = opciones4.getCheckedRadioButtonId() == R.id.r4o3 ? result+1: result-1;
        result = opciones5.getCheckedRadioButtonId() == R.id.r5o4 ? result+1: result-1;
        result = opciones6.getCheckedRadioButtonId() == R.id.r6o3 ? result+1: result-1;
        result = opciones7.getCheckedRadioButtonId() == R.id.r7o4 ? result+1: result-1;
        result = opciones8.getCheckedRadioButtonId() == R.id.r8o2 ? result+1: result-1;
        result = opciones9.getCheckedRadioButtonId() == R.id.r9o4 ? result+1: result-1;
        result = opciones10.getCheckedRadioButtonId() == R.id.r10o2 ? result+1: result-1;

        result = r12o1.isChecked() && r12o2.isChecked() ? result+1: result-1;
        result = r13o1.isChecked() && r13o3.isChecked() ? result+1: result-1;
        result = r14o2.isChecked() && r14o3.isChecked() ? result+1: result-1;
        result = r15o1.isChecked() && r15o2.isChecked() ? result+1: result-1;
        result = r16o1.isChecked() && r16o2.isChecked() ? result+1: result-1;
        result = r17o1.isChecked() && r17o3.isChecked() ? result+1: result-1;
        if(result<0)
            resultado.setText("Score: 0 out of 20");
        else
            resultado.setText("Score: "+String.valueOf(result)+" out of 20");
    }
}
